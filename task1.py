def reverse_text(text_to_reverse):
    letter_lists = str_to_letter_list(text_to_reverse)
    reversed_words_list = reverse_words(letter_lists)
    reversed_text = make_str(reversed_words_list)
    return reversed_text


def make_str(word_list):

    words = []
    for word in word_list:
        words.append("".join(word))

    return (" ".join(words))


def reverse_words(letter_lists):
    reversed_words = []

    for word in letter_lists:
        letter_list = []
        literal_dict = {}
        literal_qty = 0

        for letter in word:
            if letter.isalpha():
                letter_list.insert(0, letter)
                literal_qty += 1

            else:
                literal_dict[letter] = literal_qty
                literal_qty += 1

        full_word = input_literals(letter_list, literal_dict)
        reversed_words.append(full_word)

    return reversed_words


def input_literals(letter_list, literal_dict):

    keys = literal_dict.keys()

    for literal in keys:
        index = literal_dict.get(literal)
        letter_list.insert(index, literal)

    return letter_list

def str_to_letter_list(text):
    word_list = text.split()
    word_by_letters = []

    for word in word_list:
        letter_list = []

        for letter in word:
            letter_list.append(letter)

        word_by_letters.append(letter_list)

    return word_by_letters


def list_to_str(words_in_lists):
    string = ""

    for word_in_list in words_in_lists:
        for letter in word_in_list:
            string += letter

    return string


if __name__ == "__main__":
    cases = [("a1bc#d efg!h#", "d1cb#a hgf!e#"),
             ("", "")]

    for text, reversed_text in cases:
        assert reverse_text(text) == reversed_text
